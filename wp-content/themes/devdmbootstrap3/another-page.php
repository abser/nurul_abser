<?php
/**
 * Another Page Template
 *
 * Template Name:  Another Page
 *
 * @file           another-page.php
 * @author         Nurul Abser
 * @version        1.0.0
 */
 ?>
 <?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>

<!-- start content container -->
<div class="row dmbs-content">

    <?php //left sidebar ?>
    <?php get_sidebar( 'left' ); ?>
	
		<div class="col-md-12">
			This is Another Page ....
		</div>
		

    <?php //get the right sidebar ?>
    <?php get_sidebar( 'right' ); ?>

</div>
<!-- end content container -->

<?php get_footer(); ?>

