<?php
/**
 * News Feed Template
 *
 * Template Name:  News Feed
 *
 * @file           newsfeed.php
 * @author         Nurul Abser
 * @version        1.0.0
 */
 ?>
 <?php get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<?php get_template_part('template-part', 'topnav'); ?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
/*
*  How to find a feed based on a query.
*/

google.load("feeds", "1");

function initialize() {
  var feed1 = new google.feeds.Feed("http://feeds.bbci.co.uk/news/rss.xml");
  var feed2 = new google.feeds.Feed("http://feeds.bbci.co.uk/news/business/rss.xml");
  var feed3 = new google.feeds.Feed("http://feeds.bbci.co.uk/news/technology/rss.xml");
  
  feed1.setNumEntries(5);
  feed2.setNumEntries(5);
  feed3.setNumEntries(5);
  
  feed1.load(function(result) {
    if (!result.error) {
      var container = document.getElementById("feed1");
      var html = '';

      for (var i = 0; i < result.feed.entries.length; i++) {
      var entry = result.feed.entries[i];
      html += '<p><a target="_blank" href="' + entry.link + '">' + entry.title + '</a></p>';
    }
    container.innerHTML = html;
      }
    });
    
    
    feed2.load(function(result) {
    if (!result.error) {
      var container = document.getElementById("feed2");
      var html = '';

      for (var i = 0; i < result.feed.entries.length; i++) {
      var entry = result.feed.entries[i];
      html += '<p><a target="_blank" href="' + entry.link + '">' + entry.title + '</a></p>';
    }
    container.innerHTML = html;
      }
    });
    
    
    
    feed3.load(function(result) {
    if (!result.error) {
      var container = document.getElementById("feed3");
      var html = '';

      for (var i = 0; i < result.feed.entries.length; i++) {
      var entry = result.feed.entries[i];
      html += '<p><a target="_blank" href="' + entry.link + '">' + entry.title + '</a></p>';
    }
    container.innerHTML = html;
      }
    });
  //setInterval(initialize , 60000);
  }
  
google.setOnLoadCallback(initialize);
        
</script>
<!-- start content container -->
<div class="row dmbs-content">

    <?php //left sidebar ?>
    <?php get_sidebar( 'left' ); ?>
    

    <div class="col-md-<?php devdmbootstrap3_main_content_width(); ?> dmbs-main">
	
		<div class="col-md-4">
			<h2>Top Stories</h2>
                        <div id="feed1"></div>
		</div>
		 
		 <div class="col-md-4">
			<h2> Business</h2>
                        <div id="feed2"></div>
		</div>
		
		 	<div class="col-md-4">
			<h2>Technology</h2>
                        <div id="feed3"></div>
		</div>
		 

    </div>

    <?php //get the right sidebar ?>
    <?php get_sidebar( 'right' ); ?>

</div>
<!-- end content container -->

<?php get_footer(); ?>

